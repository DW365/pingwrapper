﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PingWrapper
{
    static class ModeController
    {
        static bool compactMode;
        static ModeController()
        {
            compactMode = !Globals.full;
            SetCurrentMode();
        }
        public static void ChangeMode()
        {
            if (compactMode)
                SetFullMode();
            else
                SetCompactMode();
            compactMode = !compactMode;
        }
        public static void SetLeft()
        {
            int x, y;
            y = Program.form1.Location.Y;
            x = Program.form1.Location.X - Program.form2.Width;
            Program.form2.Location = new System.Drawing.Point(x, y);
        }
        public static void SetRight()
        {
            int x, y;
            y = Program.form1.Location.Y;
            x = Program.form1.Location.X + Program.form1.Width;
            Program.form2.Location = new System.Drawing.Point(x, y);
        }
        public static void SetPosition()
        {
            if (Program.form1.Location.X + Program.form1.Width + Program.form2.Width > Screen.PrimaryScreen.Bounds.Width)
                SetLeft();
            else
                SetRight();
        }
        private static void SetFullMode()
        {
            SetPosition();
            Program.form2.Show();
        }
        private static void SetCompactMode()
        {
            Program.form2.Visible = false;
        }
        public static void SetCurrentMode()
        {
            if (compactMode)
                SetCompactMode();
            else
                SetFullMode();
        }
    }
}
