﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace PingWrapper
{
    class Data
    {
        List<string> data;
        public Data()
        {
            data = new List<string>();
        }
        public void Clear()
        {
            data.Clear();
            for (int i = 0; i < 50; i++)
                DrawLine(i, ColorTranslator.FromHtml(Settings.config.background_color1), 0, 50);
        }
        private void DrawData()
        {
            for(int i = 0; i < 50 && i < data.Count; i++)
            {
                Parser parser = new Parser(data[data.Count - 1 - i]);
                if ((i + data.Count) % 2 == 0 ) DrawLine(50 - i -1, Color.Black, 1, 0);
                else DrawLine(50 - i -1, Color.White, 1, 0);
                DrawLine(50 - i - 1, parser.Color(), parser.Length(), 1);
                DrawLine(50 - i - 1, ColorTranslator.FromHtml(Settings.config.background_color1), 50, parser.Length());
            }
        }
        public void Push(string msg)
        {
            data.Add(msg);
            DrawData();
        }
        private void DrawLine(int lineNumber, Color color, int length, int from)
        {
            try
            {
                System.Drawing.Pen myPen;
                int m = Convert.ToInt32(Settings.config.size_multiplier);
                myPen = new System.Drawing.Pen(color, m);
                System.Drawing.Graphics formGraphics = Program.form1.CreateGraphics();
                formGraphics.DrawLine(myPen, from * m, lineNumber * m + m / 2, length * m, lineNumber * m + m / 2);
                myPen.Dispose();
                formGraphics.Dispose();
            }
            catch (Exception e) { }
        }
    }
}
