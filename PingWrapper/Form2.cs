﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace PingWrapper
{
    partial class Form2 : Form
    {
        private int iFormX, iFormY, iMouseX, iMouseY; //Отвечают за перетаскивание формы
        public PingHolder pingHolder; //ping.exe 
        Data data;
        public Form2()
        {
            StartPosition = FormStartPosition.Manual;
            InitializeComponent();
            Show();
            Visible = false;
            BackColor = System.Drawing.ColorTranslator.FromHtml(Settings.config.background_color1);
            Text = Globals.param;
            data = new Data();
            pingHolder = new PingHolder(Globals.param, Add, this);
            richTextBox1.Font = new System.Drawing.Font(richTextBox1.Font.FontFamily, Convert.ToInt32(Settings.config.text_size));
            richTextBox1.BackColor = System.Drawing.ColorTranslator.FromHtml(Settings.config.background_color2);
            HideFromAltTab(this.Handle);
            string clean = ""+(char)Convert.ToInt32(Settings.config.key_config.clean);
            if (Convert.ToInt32(Settings.config.key_config.clean) == 27) clean = "esc";
            button1.Text += " [" + (char)Convert.ToInt32(Settings.config.key_config.stats) + "]";
            button2.Text += " [" + (char)Convert.ToInt32(Settings.config.key_config.pause) + "]";
            button3.Text += " [" + (char)Convert.ToInt32(Settings.config.key_config.save) + "]";
            button4.Text += " [" + clean + "]";
            button5.Text += " [" + (char)Convert.ToInt32(Settings.config.key_config.time) + "]";
            button6.Text += " [" + (char)Convert.ToInt32(Settings.config.key_config.copy) + "]";
            button8.Text += " [" + (char)Convert.ToInt32(Settings.config.key_config.top) + "]";
            this.KeyPreview = true;
            if (Globals.top)
            {
                button8.Text = button8.Text.Replace("Fix", "Fixed");
                Program.form1.TopMost = true;
                this.TopMost = true;
            }
        }
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr window, int index, int value);
 
        [DllImport("user32.dll")]
        private static extern int GetWindowLong(IntPtr window, int index);
 
        private const int GWL_EXSTYLE = -20;
        private const int WS_EX_TOOLWINDOW = 0x00000080;
 
        public static void HideFromAltTab(IntPtr Handle)
        {
            SetWindowLong(Handle, GWL_EXSTYLE, GetWindowLong(Handle,
                GWL_EXSTYLE) | WS_EX_TOOLWINDOW);
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            pingHolder.Exit();
            Application.Exit();
        }

        private void Add(string message)
        {
            Action<string> add = Add;
            if (richTextBox1.InvokeRequired) richTextBox1.Invoke(add, message);
            else
            {
                Parser parser = new Parser(message);
                if (!parser.Ignore()) data.Push(message);
                richTextBox1.SelectionStart = richTextBox1.TextLength;
                richTextBox1.SelectionLength = 0;
                richTextBox1.SelectionColor = parser.Color();
                richTextBox1.AppendText(parser.Text());
                richTextBox1.SelectionColor = richTextBox1.ForeColor;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Location = Program.form1.Location;
        }
        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            richTextBox1.SelectionStart = richTextBox1.TextLength;
            richTextBox1.ScrollToCaret();
        }

        private void Form2_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Middle) Environment.Exit(0);
            iFormX = this.Location.X;
            iFormY = this.Location.Y;
            iMouseX = MousePosition.X;
            iMouseY = MousePosition.Y;
        }

        private void Form2_MouseMove(object sender, MouseEventArgs e)
        {
            int iMouseX2 = MousePosition.X;
            int iMouseY2 = MousePosition.Y;
            if (e.Button == MouseButtons.Left)
            {
                this.Location = new Point(iFormX + (iMouseX2 - iMouseX), iFormY + (iMouseY2 - iMouseY));
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        public void Pause()
        {
            if (button2.Text[0] == 'P') button2.Text = button2.Text.Replace("Pause", "Res");
            else
                if (button2.Text[0] == 'R') button2.Text = button2.Text.Replace("Res", "Pause");
            System.Diagnostics.Debug.WriteLine(Convert.ToString(button2.Text[0] == 'P'));
            pingHolder.PlayPause();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Pause();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            pingHolder.Stats();
        }
        public void Save()
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.FileName = String.Format("{0:H_mm_ss}", DateTime.Now);
            sfd.Filter = "Текстовый документ (*.txt)|*.txt|Все файлы (*.*)|*.*";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(sfd.FileName))
                {
                    foreach (var line in richTextBox1.Lines)
                        file.WriteLine(line);
                }
            }     
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Save();      
        }

        private void Form2_Load(object sender, EventArgs e)
        {
        }
        public bool IsEmpty()
        {
            if (richTextBox1.Text == "") return true;
            return false;
        }
        public void Clear()
        {
            richTextBox1.Text = "";
            data.Clear();
        }
        private void button4_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Globals.stampEnabled = !Globals.stampEnabled;
        }
        public void Copy()
        {
            if (richTextBox1.Text.Length > 0)
                Clipboard.SetText(richTextBox1.Text);
        }
        private void button6_Click(object sender, EventArgs e)
        {
            Copy();
        }

        private void richTextBox1_Enter(object sender, EventArgs e)
        {
            
        }

        private void richTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void Form2_Activated(object sender, EventArgs e)
        {

        }
        public void SetTop()
        {
            Program.form1.TopMost = !Program.form1.TopMost;
            Program.form2.TopMost = !Program.form2.TopMost;
            if(Program.form1.TopMost)
            {
                button8.Text = button8.Text.Replace("Fix", "Fixed");
            }
            else
                button8.Text = button8.Text.Replace("Fixed", "Fix");
        }
        private void button8_Click(object sender, EventArgs e)
        {
            SetTop();
        }

        private void Form2_KeyPress(object sender, KeyPressEventArgs e)
        {
            string k = Convert.ToString((int)e.KeyChar);
            if (Settings.config.key_config.mode == k) ModeController.ChangeMode();
            if (Settings.config.key_config.clean == k) Program.form2.Clear();
            if (Settings.config.key_config.copy == k) Program.form2.Copy();
            if (Settings.config.key_config.pause == k) Program.form2.Pause();
            if (Settings.config.key_config.save == k) Program.form2.Save();
            if (Settings.config.key_config.stats == k) pingHolder.Stats();
            if (Settings.config.key_config.time == k) Globals.stampEnabled = !Globals.stampEnabled;
            if (Settings.config.key_config.top == k) Program.form2.SetTop();
        }

        private void richTextBox1_MouseClick(object sender, MouseEventArgs e)
        {
            Pause();
        }

        private void Form2_VisibleChanged(object sender, EventArgs e)
        {
        }
    }
}
